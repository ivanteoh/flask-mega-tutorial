# -*- coding: utf-8 -*-
"""Error handling blueprint."""

from flask import Blueprint

bp = Blueprint('errors', __name__)

from app.errors import handlers  # pylint: disable=C0413
