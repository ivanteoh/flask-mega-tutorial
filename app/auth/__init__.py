# -*- coding: utf-8 -*-
"""Authentication blueprint."""

from flask import Blueprint

bp = Blueprint('auth', __name__)

from app.auth import routes  # pylint: disable=C0413
