# -*- coding: utf-8 -*-
"""Main application blueprint."""

from flask import Blueprint

bp = Blueprint('main', __name__)

from app.main import routes  # pylint: disable=C0413
