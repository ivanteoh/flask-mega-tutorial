# -*- coding: utf-8 -*-
"""The auth token api."""
from flask import g
from flask import jsonify
from app import db
from app.api import bp
from app.api.auth import basic_auth
from app.api.auth import token_auth


@bp.route('/tokens', methods=['POST'])
@basic_auth.login_required
def get_token():
    """Get auth token."""
    token = g.current_user.get_token()
    db.session.commit()
    return jsonify({'token': token})


@bp.route('/tokens', methods=['DELETE'])
@token_auth.login_required
def revoke_token():
    """Revoke auth token."""
    g.current_user.revoke_token()
    db.session.commit()
    return '', 204
