# -*- coding: utf-8 -*-
"""The begin of the app api.

Summary
+=============+===========================+===================================+
| HTTP Method | Resource URL              | Notes                             |
+=============+===========================+===================================+
| GET         | /api/users/<id>           | Return a user.                    |
+-------------+---------------------------+-----------------------------------+
| GET         | /api/users                | Return the collection of all      |
|             |                           | users.                            |
+-------------+---------------------------+-----------------------------------+
| GET         | /api/users/<id>/followers | Return the collection of          |
|             |                           | followers of this user.           |
+-------------+---------------------------+-----------------------------------+
| GET         | /api/users/<id>/followed  | Return the collection of users    |
|             |                           | this user is following.           |
+-------------+---------------------------+-----------------------------------+
| POST        | /api/users                | Register a new user account. User |
|             |                           | representation given in the body. |
+-------------+---------------------------+-----------------------------------+
| PUT         | /api/users/<id>           | Modify a user. Only allowed to be |
|             |                           | issued by the user itself.        |
+=============+===========================+===================================+
"""
from flask import Blueprint

bp = Blueprint('api', __name__)

from app.api import users, errors, tokens
