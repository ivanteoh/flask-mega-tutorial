# The Flask Mega-Tutorial

Tutorial from https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world to learn how to create web applications with Python and the Flask framework.

Author: Miguel Grinberg
Date: Dec 06, 2017
Tags: python flask

## To run the application

Run these commands in your terminal console.

```bash
$ source ./venv/bin/activate
$ pip install -r requirements.txt
$ export FLASK_APP=microblog.py
$ export SECRET_KEY=my_secret_key
```

Everything if there is change in the model, you need to run these command

```bash
$ flask db migrate -m "new message"
```

Run these commands to generate db structures.

```bash
$ flask db upgrade
```

Debug the application by using flask shell.

```bash
$ flask shell
```

Start the application.

```bash
$ flask run
```

Turn on the debug mode

```bash
$ export FLASK_DEBUG=1
```

## Email server

1. Create a fake email server, by running the following command on
seperate terminal section

```
$ python -m smtpd -n -c DebuggingServer localhost:8025
```

```bash
$ export MAIL_SERVER=localhost
$ export MAIL_PORT=8025
$ export FLASK_DEBUG=0
```

2. Using external email server

```bash
$ export MAIL_SERVER=smtp.sendgrid.net
$ export MAIL_PORT=587
$ export MAIL_USE_TLS=1
$ export MAIL_USERNAME=<your-sendgrid-username>
$ export MAIL_PASSWORD=<your-sendgrid-password>
$ export ADMIN_EMAIL=<your-email-address>
```

## Testing

Running the entire test suite

```bash
$ python tests.py
```

## Translation

### Using `pybabel`
 
To extract all the texts to the `.pot` file

```bash
$ pybabel extract -F babel.cfg -k _l -o messages.pot .
```

Create a translation for Spanish language (language code es) 

```bash
$ pybabel init -i messages.pot -d app/translations -l es
```

To compile all the translations for the application

```bash
$ pybabel compile -d app/translations
```

To update the translations by generating a new version of messages.pot
The `update` call takes the new `messages.pot` file and merges it into all the `messages.po` files associated with the project.

```bash
$ pybabel extract -F babel.cfg -k _l -o messages.pot .
$ pybabel update -i messages.pot -d app/translations
```

### Using `flask` command

To add a new language

```bash
$ flask translate init <language-code>
```

To update all the languages after making changes to the _() and _l() language markers

```bash
$ flask translate update
```

To compile all languages after updating the translation files

```bash
$ flask translate compile
```

# Translation API

Enter your `Key 1` or `Key 2` into an environment variable

```bash
$ export MS_TRANSLATOR_KEY=<paste-your-key-here>
```

# Environment variables

You can add all the configuration-time variables at `.env` file in the root application directory except `FLASK_APP` and `FLASK_DEBUG`.

# Full text search

Running elasticsearch in macOS using brew

```bash
$ brew install elasticsearch
$ brew info elasticsearch
$ brew services start elasticsearch
```

# Deployment

## When using Vagrant

```bash
$ vagrant up
$ vagrant ssh
```

## Password-less logins

Login to server

```bash
$ ssh root@<server-ip-address>
```

Create non root account named `ubuntu`

```bash
$ adduser --gecos "" ubuntu
$ usermod -aG sudo ubuntu
$ su ubuntu
```

Paste pubic key to server

```bash
$ echo <paste-your-pubic-key-here> >> ~/.ssh/authorized_keys
$ chmod 600 ~/.ssh/authorized_keys
```

Login using non root user without password

```bash
$ ssh ubuntu@<server-ip-address>
```

## Secure your server

Update `/etc/ssh/sshd_config`

a. Disable root logins

```text
PermitRootLogin no
```

b. Disable password logins

```text
PasswordAuthentication no
```

Restarted for the changes

```bash
$ sudo service ssh restart
```

Install a firewall

```bash
$ sudo apt-get install -y ufw
$ sudo ufw allow ssh
$ sudo ufw allow http
$ sudo ufw allow 443/tcp
$ sudo ufw --force enable
$ sudo ufw status
```

Install base dependencies

```bash
$ sudo apt-get -y update
$ sudo apt-get -y install python3 python3-venv python3-dev
$ sudo apt-get -y install mysql-server postfix supervisor nginx git
```

Install the application

```bash
$ git clone https://github.com/miguelgrinberg/microblog
$ cd microblog
$ git checkout v0.17
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
(venv) $ pip install gunicorn pymysql
```

Add database url to `.env` file

```text
DATABASE_URL=mysql+pymysql://microblog:<db-password>@localhost:3306/microblog
```

To generate random string for `SECRET_KEY`

```bash
$ python3 -c "import uuid; print(uuid.uuid4().hex)"
```

Update `.profile` with `FLASK_APP` environment variable

```bash
$ echo "export FLASK_APP=microblog.py" >> ~/.profile
```

Compile the language translations

```bash
(venv) $ flask translate compile
```

Setup MySQL

```bash
$ mysql -u root -p
```

Create the database

```mysql
mysql> create database microblog character set utf8 collate utf8_bin;
mysql> create user 'microblog'@'localhost' identified by '<db-password>';
mysql> grant all privileges on microblog.* to 'microblog'@'localhost';
mysql> flush privileges;
mysql> quit;
```

Upgrade the database with models

```bash
(venv) $ flask db upgrade
```

Setup Gunicorn

```bash
(venv) $ gunicorn -b localhost:8000 -w 4 microblog:app
```

Setup Supervisor configuration file at `/etc/supervisor/conf.d/microblog.conf`
and then restart the service

```bash
$ sudo supervisorctl reload
```

Setup Nginx

```bash
$ mkdir certs
$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
  -keyout certs/key.pem -out certs/cert.pem
```

Remove default test site

```bash
$ sudo rm /etc/nginx/sites-enabled/default
```

Update configuration file at `/etc/nginx/sites-enabled/microblog`
and then restart the service

```bash
$ sudo service nginx reload
```

Using free `Let's Encrypt` SSL certificate, refer to https://blog.miguelgrinberg.com/post/running-your-flask-application-over-https

Upgrade application

```bash
(venv) $ git pull                              # download the new version
(venv) $ sudo supervisorctl stop microblog     # stop the current server
(venv) $ flask db upgrade                      # upgrade the database
(venv) $ flask translate compile               # upgrade the translations
(venv) $ sudo supervisorctl start microblog    # start a new server
```

## Deployment on Heroku

Login to Heroku

```bash
$ brew install heroku/brew/heroku
$ heroku login
```

Setup Git

```bash
$ git clone https://github.com/miguelgrinberg/microblog
$ cd microblog
$ git checkout v0.18
$ heroku apps:create <unique-id>
Creating <unique-id>... done
http://<unique-id>.herokuapp.com/ | https://git.heroku.com/<unique-id>.git
$ git remote -v
heroku	https://git.heroku.com/<unique-id>.git (fetch)
heroku	https://git.heroku.com/<unique-id>.git (push)
```

Heroku Postgres Database

```bash
$ heroku addons:add heroku-postgresql:hobby-dev
Creating heroku-postgresql:hobby-dev on flask-microblog... free
Database has been created and is available
 ! This database is empty. If upgrading, you can transfer
 ! data from another database with pg:copy
Created postgresql-parallel-56076 as DATABASE_URL
Use heroku addons:docs heroku-postgresql to view documentation
```

Logging to stdout

```bash
$ heroku config:set LOG_TO_STDOUT=1
Setting LOG_TO_STDOUT and restarting <unique-id>... done, v4
LOG_TO_STDOUT: 1
```

Elasticsearch hosting

```bash
$ heroku addons:create searchbox:starter
$ heroku config:get SEARCHBOX_URL
<your-elasticsearch-url>
$ heroku config:set ELASTICSEARCH_URL=<your-elasticsearch-url>
```

Add `FLASK_APP` environment variable

```bash
$ heroku config:set FLASK_APP=microblog.py
Setting FLASK_APP and restarting <unique-id>... done, v4
FLASK_APP: microblog.py
```

Deploy to Heroku

```bash
$ git push heroku master
```

## Deployment on Docker Containers

Build the container image

```bash
$ docker build -t microblog:latest .
```

Start the container

```bash
$ docker run --name microblog -d -p 8000:5000 --rm microblog:latest
```

Or with run-time environment variables

```bash
$ docker run --name microblog -d -p 8000:5000 --rm -e SECRET_KEY=my-secret-key \
    -e MAIL_SERVER=smtp.googlemail.com -e MAIL_PORT=587 -e MAIL_USE_TLS=true \
    -e MAIL_USERNAME=<your-gmail-username> -e MAIL_PASSWORD=<your-gmail-password> \
    microblog:latest
```

See what containers are running

```bash
$ docker ps
```

Starts a mySQL server

```bash
$ docker run --name mysql -d -e MYSQL_RANDOM_ROOT_PASSWORD=yes \
    -e MYSQL_DATABASE=microblog -e MYSQL_USER=microblog \
    -e MYSQL_PASSWORD=<database-password> \
    mysql/mysql-server:5.7
```

Link to MySQL server

```bash
$ docker run --name microblog -d -p 8000:5000 --rm -e SECRET_KEY=my-secret-key \
    -e MAIL_SERVER=smtp.googlemail.com -e MAIL_PORT=587 -e MAIL_USE_TLS=true \
    -e MAIL_USERNAME=<your-gmail-username> -e MAIL_PASSWORD=<your-gmail-password> \
    --link mysql:dbserver \
    -e DATABASE_URL=mysql+pymysql://microblog:<database-password>@dbserver/microblog \
    microblog:latest
```

Adding a Elasticsearch Container

```bash
$ docker run --name elasticsearch -d -p 9200:9200 -p 9300:9300 --rm \
    -e "discovery.type=single-node" \
    docker.elastic.co/elasticsearch/elasticsearch-oss:6.1.1
```

Link with MySQL and Elasticsearch Container

```bash
$ docker run --name microblog -d -p 8000:5000 --rm -e SECRET_KEY=my-secret-key \
    -e MAIL_SERVER=smtp.googlemail.com -e MAIL_PORT=587 -e MAIL_USE_TLS=true \
    -e MAIL_USERNAME=<your-gmail-username> -e MAIL_PASSWORD=<your-gmail-password> \
    --link mysql:dbserver \
    -e DATABASE_URL=mysql+pymysql://microblog:<database-password>@dbserver/microblog \
    --link elasticsearch:elasticsearch \
    -e ELASTICSEARCH_URL=http://elasticsearch:9200 \
    microblog:latest
```

See logs for the containers

```bash
$ docker logs microblog
```

Publish your image to the Docker registry

```bash
$ docker login
$ docker tag microblog:latest <your-docker-registry-account>/microblog:latest
$ docker push <your-docker-registry-account>/microblog:latest
```

# Background Jobs

Install redis in macOSX and start the service

```bash
$ brew install redis
$ redis-server
```

Run the redis queue worker

```bash
(venv) $ rq worker microblog-tasks
```

Install in linux

```bash
$ sudo apt-get install redis-server
```

Create another Supervisor configuration

```text
rq worker microblog-tasks
```

## Heroku deployment

```bash
$ heroku addons:create heroku-redis:hobby-dev
```

Start worker in Heroku

```bash
$ heroku ps:scale worker=1
```

## Docker deployment

Run redis image

```bash
$ docker run --name redis -d -p 6379:6379 redis:3-alpine
```

Run the application and link with redis

```bash
$ docker run --name microblog -d -p 8000:5000 --rm -e SECRET_KEY=my-secret-key \
    -e MAIL_SERVER=smtp.googlemail.com -e MAIL_PORT=587 -e MAIL_USE_TLS=true \
    -e MAIL_USERNAME=<your-gmail-username> -e MAIL_PASSWORD=<your-gmail-password> \
    --link mysql:dbserver --link redis:redis-server \
    -e DATABASE_URL=mysql+pymysql://microblog:<database-password>@dbserver/microblog \
    -e REDIS_URL=redis://redis-server:6379/0 \
    microblog:latest
```

Start a rq worker

```bash
$ docker run --name rq-worker -d --rm -e SECRET_KEY=my-secret-key \
    -e MAIL_SERVER=smtp.googlemail.com -e MAIL_PORT=587 -e MAIL_USE_TLS=true \
    -e MAIL_USERNAME=<your-gmail-username> -e MAIL_PASSWORD=<your-gmail-password> \
    --link mysql:dbserver --link redis:redis-server \
    -e DATABASE_URL=mysql+pymysql://microblog:<database-password>@dbserver/microblog \
    -e REDIS_URL=redis://redis-server:6379/0 \
    --entrypoint venv/bin/rq \
    microblog:latest worker -u redis://redis-server:6379/0 microblog-tasks
```
